﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class GameState : MonoBehaviour
{
    private int score;
    private State currentState;

    private void Start()
    {
        SetState(new J1(this));
    }

    private void Update()
    {
        currentState.Tick();
    }

    public void SetState(State state)
    {
        currentState = state;
    }
}

public abstract class State
{
    protected GameState gameState;

    public abstract void Tick();


    public State(GameState gameState)
    {
        this.gameState = gameState;
    }
}


public class J1 : State
{
    public J1(GameState gameState) : base(gameState){}
    public override void Tick()
    {
        Debug.Log("J1");
        gameState.SetState(new J2(gameState));
    }

}

public class J2 : State
{
    public J2(GameState gameState) : base(gameState){}

    public override void Tick()
    {
        Debug.Log("J2");
        gameState.SetState(new J1(gameState));
    }
}