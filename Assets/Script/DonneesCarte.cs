﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CardData", menuName = "Card", order = 51)]
public class DonneesCarte : ScriptableObject
{
    //Déclaration des variable privé.
    [SerializeField] private string cardName;
    [SerializeField] private string description;
    [SerializeField] private Sprite icon;
    [SerializeField] private int valeur;
    [SerializeField] private bool feu;
    [SerializeField] private bool vitesse;
    [SerializeField] private bool essence;
    [SerializeField] private bool pneu;
    [SerializeField] private bool accident;
    [SerializeField] private bool volant;
    [SerializeField] private bool prio;
    [SerializeField] private bool citerne;
    [SerializeField] private bool increvable;

    //Méthode d'appel des différents variables.
    public string CardName
    {
        get { return cardName; }
    }

    public string Description
    {
        get { return description; }
    }

    public Sprite Icon
    {
        get { return icon; }
    }

    public int Valeur
    {
        get { return valeur; }
    }

    public bool Feu
    {
        get { return feu; }
    }

    public bool Vitesse
    {
        get { return vitesse; }
    }

    public bool Essence
    {
        get { return essence; }
    }

    public bool Pneu
    {
        get { return pneu; }
    }

    public bool Accident
    {
        get { return accident; }
    }

    public bool Increvable
    {
        get { return increvable; }
    }

    public bool Citerne
    {
        get { return citerne; }
    }

    public bool Volant
    {
        get { return volant; }
    }

    public bool Prio
    {
        get { return prio; }
    }


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}