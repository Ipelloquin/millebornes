﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MelangeCarte : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject feuVert;
    public GameObject feuRouge;
    public GameObject finAccident;
    public GameObject finLimite;
    public GameObject pleinEssence;
    public GameObject pneuRechange;
    public GameObject citerne;
    public GameObject increvable;
    public GameObject prio;
    public GameObject volant;
    public GameObject borne25;
    public GameObject borne50;
    public GameObject borne75;
    public GameObject borne100;
    public GameObject borne200;
    public GameObject accident;
    public GameObject debutLimite;
    public GameObject panneEssence;
    public GameObject pneuCreve;

    public List<GameObject> pioche = new List<GameObject>();

    public Sprite cardBack;
    
    private Vector3 pos;
    
    private int numPioche = 106;
    
    void Start()
    {
        pos = GameObject.Find("Pioche").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Instantiate();
    }

    void Instantiate ()
    {
        if (numPioche > 92)
        {
            var card =  feuVert;
            card.name = "FeuVert" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 93 && numPioche > 87)
        {
            var card = feuRouge;
            card.name = "FeuRouge" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 88 && numPioche > 81)
        {
            var card = finLimite;
            card.name = "FinLimite" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 82 && numPioche > 75)
        {
            var card = finAccident;
            card.name = "FinAccident" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 76 && numPioche > 69)
        {
            var card = pleinEssence;
            card.name = "PleinEssence" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 70 && numPioche > 63)
        {
            var card = pneuRechange;
            card.name = "PneuRechange" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 64 && numPioche > 60)
        {
            var card = accident;
            card.name = "Accident" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 61 && numPioche > 56)
        {
            var card = debutLimite;
            card.name = "DebutLimite" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 57 && numPioche > 53)
        {
            var card = panneEssence;
            card.name = "PanneEssence" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 54 && numPioche > 50)
        {
            var card = pneuCreve;
            card.name = "PneuCreve" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 51 && numPioche > 49)
        {
            var card = citerne;
            card.name = "Citerne" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 50 && numPioche > 48)
        {
            var card = increvable;
            card.name = "Increvable" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 49 && numPioche > 47)
        {
            var card = prio;
            card.name = "Prio" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 48 && numPioche > 46)
        {
            var card = volant;
            card.name = "Volant" + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 47 && numPioche > 42)
        {
            var card = borne200;
            card.name = "Borne200 " + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 43 && numPioche > 30)
        {
            var card = borne100;
            card.name = "Borne100 " + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }

        if (numPioche < 31 && numPioche > 20)
        {
            var card = borne75;
            card.name = "Borne75 " + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }
        
        if (numPioche < 21 && numPioche > 10)
        {
            var card = borne50;
            card.name = "Borne50 " + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }
        
        if (numPioche < 11 && numPioche > 0)
        {
            var card = borne25;
            card.name = "Borne25 " + numPioche;
            card.GetComponent<SpriteRenderer>().sprite = cardBack;
            card.GetComponent<Carte>().pioche = numPioche;
            numPioche--;
            pioche.Add(card);
        }
    }

    void PickACard()
    {
        
    }
}